+++
title = "Course Material"

date = 2019-03-04T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
[menu.course]
  name = "Course"
  weight = 1
+++

> The course material for multiple sessions are still being updated.

All course material is found on the left side bar. Each session is listed in order 
of instruction, from the first to the last day.

Please also make sure to install these packages by pasting this into the RStudio
Console:

```r
install.packages(c("usethis", "prodigenr", "furrr", "fs", "styler", "here", 
                   "tidyverse", "NHANES"))
```

You can also install by opening RStudio, click the "Packages" tab, click
"Install", and type out each of the packages. If you need help, we will make sure
that everyone will install these before the sessions begin.

The original R Markdown teaching material can be found in the [GitLab
repository]. Anyone is free to use, re-use, modify, and so on (as per the
[license](/license/)) as long as you properly attribute the work. Please see the
["How to cite"] section of the README in the repository.

[GitLab repository]: https://gitlab.com/lwjohnst/rqawr-rcourse
["How to cite"]: https://gitlab.com/lwjohnst/rqawr-rcourse#how-to-cite-the-material
