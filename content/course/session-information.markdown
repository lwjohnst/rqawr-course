+++
title = "Package and software information"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
linktitle = "R information"
[menu.course]
  parent = "Course"
  weight = 1000
+++

This contains information on the package versions used to generate the material
used in this website.




```r
library(tidyverse)
#> Registered S3 methods overwritten by 'ggplot2':
#>   method         from 
#>   [.quosures     rlang
#>   c.quosures     rlang
#>   print.quosures rlang
#> ── Attaching packages ─────────────────────────── tidyverse 1.2.1 ──
#> ✔ ggplot2 3.1.1     ✔ purrr   0.3.2
#> ✔ tibble  2.1.1     ✔ dplyr   0.8.1
#> ✔ tidyr   0.8.3     ✔ stringr 1.4.0
#> ✔ readr   1.3.1     ✔ forcats 0.4.0
#> ── Conflicts ────────────────────────────── tidyverse_conflicts() ──
#> ✖ dplyr::filter() masks stats::filter()
#> ✖ dplyr::lag()    masks stats::lag()
library(usethis)
library(prodigenr)
library(rmarkdown)
library(styler)
sessioninfo::session_info()
#> ─ Session info ──────────────────────────────────────────────────────────
#>  setting  value                       
#>  version  R version 3.6.0 (2019-04-26)
#>  os       Ubuntu 18.04.2 LTS          
#>  system   x86_64, linux-gnu           
#>  ui       X11                         
#>  language (EN)                        
#>  collate  en_US.UTF-8                 
#>  ctype    en_US.UTF-8                 
#>  tz       Europe/Copenhagen           
#>  date     2019-05-20                  
#> 
#> ─ Packages ──────────────────────────────────────────────────────────────
#>  package     * version date       lib source        
#>  assertthat    0.2.1   2019-03-21 [1] CRAN (R 3.6.0)
#>  backports     1.1.4   2019-04-10 [1] CRAN (R 3.6.0)
#>  blogdown      0.12    2019-05-01 [1] CRAN (R 3.6.0)
#>  bookdown      0.10    2019-05-10 [1] CRAN (R 3.6.0)
#>  broom         0.5.2   2019-04-07 [1] CRAN (R 3.6.0)
#>  cellranger    1.1.0   2016-07-27 [1] CRAN (R 3.6.0)
#>  cli           1.1.0   2019-03-19 [1] CRAN (R 3.6.0)
#>  colorspace    1.4-1   2019-03-18 [1] CRAN (R 3.6.0)
#>  crayon        1.3.4   2017-09-16 [1] CRAN (R 3.6.0)
#>  digest        0.6.19  2019-05-20 [1] CRAN (R 3.6.0)
#>  dplyr       * 0.8.1   2019-05-14 [1] CRAN (R 3.6.0)
#>  evaluate      0.13    2019-02-12 [1] CRAN (R 3.6.0)
#>  forcats     * 0.4.0   2019-02-17 [1] CRAN (R 3.6.0)
#>  fs            1.3.1   2019-05-06 [1] CRAN (R 3.6.0)
#>  generics      0.0.2   2018-11-29 [1] CRAN (R 3.6.0)
#>  ggplot2     * 3.1.1   2019-04-07 [1] CRAN (R 3.6.0)
#>  glue          1.3.1   2019-03-12 [1] CRAN (R 3.6.0)
#>  gtable        0.3.0   2019-03-25 [1] CRAN (R 3.6.0)
#>  haven         2.1.0   2019-02-19 [1] CRAN (R 3.6.0)
#>  hms           0.4.2   2018-03-10 [1] CRAN (R 3.6.0)
#>  htmltools     0.3.6   2017-04-28 [1] CRAN (R 3.6.0)
#>  httr          1.4.0   2018-12-11 [1] CRAN (R 3.6.0)
#>  jsonlite      1.6     2018-12-07 [1] CRAN (R 3.6.0)
#>  knitr         1.23    2019-05-18 [1] CRAN (R 3.6.0)
#>  lattice       0.20-38 2018-11-04 [4] CRAN (R 3.5.1)
#>  lazyeval      0.2.2   2019-03-15 [1] CRAN (R 3.6.0)
#>  lubridate     1.7.4   2018-04-11 [1] CRAN (R 3.6.0)
#>  magrittr      1.5     2014-11-22 [1] CRAN (R 3.6.0)
#>  modelr        0.1.4   2019-02-18 [1] CRAN (R 3.6.0)
#>  munsell       0.5.0   2018-06-12 [1] CRAN (R 3.6.0)
#>  nlme          3.1-139 2019-04-09 [4] CRAN (R 3.5.3)
#>  pillar        1.4.0   2019-05-11 [1] CRAN (R 3.6.0)
#>  pkgconfig     2.0.2   2018-08-16 [1] CRAN (R 3.6.0)
#>  plyr          1.8.4   2016-06-08 [1] CRAN (R 3.6.0)
#>  prodigenr   * 0.4.0   2018-05-23 [1] CRAN (R 3.6.0)
#>  purrr       * 0.3.2   2019-03-15 [1] CRAN (R 3.6.0)
#>  R6            2.4.0   2019-02-14 [1] CRAN (R 3.6.0)
#>  Rcpp          1.0.1   2019-03-17 [1] CRAN (R 3.6.0)
#>  readr       * 1.3.1   2018-12-21 [1] CRAN (R 3.6.0)
#>  readxl        1.3.1   2019-03-13 [1] CRAN (R 3.6.0)
#>  rlang         0.3.4   2019-04-07 [1] CRAN (R 3.6.0)
#>  rmarkdown   * 1.12    2019-03-14 [1] CRAN (R 3.6.0)
#>  rstudioapi    0.10    2019-03-19 [1] CRAN (R 3.6.0)
#>  rvest         0.3.4   2019-05-15 [1] CRAN (R 3.6.0)
#>  scales        1.0.0   2018-08-09 [1] CRAN (R 3.6.0)
#>  sessioninfo   1.1.1   2018-11-05 [1] CRAN (R 3.6.0)
#>  stringi       1.4.3   2019-03-12 [1] CRAN (R 3.6.0)
#>  stringr     * 1.4.0   2019-02-10 [1] CRAN (R 3.6.0)
#>  styler      * 1.1.1   2019-05-06 [1] CRAN (R 3.6.0)
#>  tibble      * 2.1.1   2019-03-16 [1] CRAN (R 3.6.0)
#>  tidyr       * 0.8.3   2019-03-01 [1] CRAN (R 3.6.0)
#>  tidyselect    0.2.5   2018-10-11 [1] CRAN (R 3.6.0)
#>  tidyverse   * 1.2.1   2017-11-14 [1] CRAN (R 3.6.0)
#>  usethis     * 1.5.0   2019-04-07 [1] CRAN (R 3.6.0)
#>  withr         2.1.2   2018-03-15 [1] CRAN (R 3.6.0)
#>  xfun          0.7     2019-05-14 [1] CRAN (R 3.6.0)
#>  xml2          1.2.0   2018-01-24 [1] CRAN (R 3.6.0)
#> 
#> [1] /home/luke/R/x86_64-pc-linux-gnu-library/3.6
#> [2] /usr/local/lib/R/site-library
#> [3] /usr/lib/R/site-library
#> [4] /usr/lib/R/library
```
