+++
title = "Data analysis in the era of reproducible and open science"

date = 2019-03-19T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

linktitle = "Era of open, reproducible science"
[menu.course]
  parent = "Course"
  url = "/slides/open-reproducible-science-era.html"
  weight = 80
+++

