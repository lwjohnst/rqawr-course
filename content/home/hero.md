+++
# Hero widget.
widget = "hero"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Reproducible Quantitative Analyses and Workflows using R"

# Order that this section will appear in.
weight = 3

# Overlay a color or image (optional).
#   Deactivate an option by commenting out the line, prefixing it with `#`.
[header]
  overlay_color = "#666"  # An HTML color value.
  overlay_img = "background.png"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.3  # Darken the image. Value in range 0-1.

# Call to action button (optional).
#   Activate the button by specifying a URL and button label below.
#   Deactivate by commenting out parameters, prefixing lines with `#`.
# TODO: See if I can use this for installing material.
# [cta]
#  url = "./post/getting-started/"
#  label = '<i class="fas fa-download"></i> Install Now'
+++

Reproducibility and open scientific practices are increasingly being requested
or required of scientists and researchers, but training on these practices has 
not kept pace. This course intends to help bridge that gap.
