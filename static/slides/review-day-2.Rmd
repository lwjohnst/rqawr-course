---
title: "Review of the second day's material"
output:
  xaringan::moon_reader:
    self_contained: true
    lib_dir: libs
    css: ["xaringan-themer.css", "../../resources/custom.css"]
    nature:
      ratio: "16:9"
      highlightStyle: xcode
      highlightLines: true
      countIncrementalSlides: false
---

layout: true

<div class="my-footer">
<span>
<img src="../img/au_logo_black.png" alt="Aarhus University", width="140">
</span>
</div> 

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
options(tibble.print_min = 4, tibble.print_max = 4)
knitr::opts_chunk$set(comment = "#>", warning = FALSE, message = FALSE,
                      fig.height = 6)
library(tidyverse)
library(NHANES)
```

```{r xaringan-themer, include=FALSE}
library(xaringanthemer)
mono_accent(
    base_color = "#2a2e44",
    header_font_google = google_font("Fira Sans"),
    text_font_google = google_font("Crimson Text"),
    code_font_google = google_font("Source Code Pro")
)
```

---

class: center, middle

# Version control with Git

---

## Using Git, basic workflow

.pull-left[
- Version control is *really* important, but also difficult
- Repository is all the files in a folder tracked by Git and saved as history in
`.git/`
- Files that are "tracked" will have changes recorded in history (when committed)
- Commit small changes and commit often
]

--

.pull-right[
1. Start tracking new files (`git add` and `git commit`; "Stage" then "Commit")
1. Check "status" of repository (`git status`, in Git tab)
1. Add and commit modified files (`git add` and `git commit`; "Stage" then "Commit")
1. Check what was recently done to the repo (`git log`; "History" tab in Git Interface)
1. Synch with GitHub, if set up (`git push` to upload, `git pull` to download;
"Push" or "Pull")
]

---

class: center, middle

# Data visualization with ggplot2

---

## Final exercise: Code to create first plot

.pull-left[
```{r final-exercise-plot1, eval=FALSE}
NHANES %>% 
    filter(!is.na(Diabetes)) %>% 
    ggplot(aes(y = BMI, x = Poverty, 
               colour = Diabetes)) + 
    geom_point(alpha = 0.4) + 
    geom_smooth(size = 2, method = "gam") +
    facet_grid(cols = vars(SurveyYr), 
               rows = vars(Gender)) +
    scale_color_viridis_d(end = 0.8) +
    theme_classic() +
    theme(
        strip.background = element_blank(),
        panel.background = 
            element_rect(fill = "grey95"),
        axis.line = 
            element_line(colour = "grey80")
    )
```
]

.pull-left[
```{r final-exercise-plot1, echo=FALSE}
```
]

---

## Final exercise: Remove NA and set base plot

.pull-left[
```{r final-exercise-plot1-1, eval=FALSE}
NHANES %>% 
    filter(!is.na(Diabetes)) %>% #<<
    ggplot(aes(y = BMI, x = Poverty, #<<
               colour = Diabetes)) + #<<
    geom_point(alpha = 0.4) + 
    geom_smooth(size = 2, method = "gam") +
    facet_grid(cols = vars(SurveyYr), 
               rows = vars(Gender)) +
    scale_color_viridis_d(end = 0.8) +
    theme_classic() +
    theme(
        strip.background = element_blank(),
        panel.background = 
            element_rect(fill = "grey95"),
        axis.line = 
            element_line(colour = "grey80")
    )
```
]

.pull-right[
```{r base-plot1, echo=FALSE}
nhanes_plot <- NHANES %>% 
    filter(!is.na(Diabetes)) %>%
    ggplot(aes(y = BMI, x = Poverty, colour = Diabetes)) 
nhanes_plot
```
]

---

## Final exercise: Add points and smooth geoms

.pull-left[
```{r final-exercise-plot1-2, eval=FALSE}
NHANES %>% 
    filter(!is.na(Diabetes)) %>%
    ggplot(aes(y = BMI, x = Poverty, 
               colour = Diabetes)) + 
    geom_point(alpha = 0.4) + #<<
    geom_smooth(size = 2, method = "gam") + #<<
    facet_grid(cols = vars(SurveyYr), 
               rows = vars(Gender)) +
    scale_color_viridis_d(end = 0.8) +
    theme_classic() +
    theme(
        strip.background = element_blank(),
        panel.background = 
            element_rect(fill = "grey95"),
        axis.line = 
            element_line(colour = "grey80")
    )
```
]

.pull-right[
```{r dots-smooth, echo=FALSE}
nhanes_plot <- nhanes_plot +
    geom_point(alpha = 0.4) + 
    geom_smooth(size = 2, method = "gam")
nhanes_plot
```
]

---

## Final exercise: Add column by row facets

.pull-left[
```{r final-exercise-plot1-3, eval=FALSE}
NHANES %>% 
    filter(!is.na(Diabetes)) %>%
    ggplot(aes(y = BMI, x = Poverty, 
               colour = Diabetes)) + 
    geom_point(alpha = 0.4) + 
    geom_smooth(size = 2, method = "gam") +
    facet_grid(cols = vars(SurveyYr), #<<
               rows = vars(Gender)) + #<<
    scale_color_viridis_d(end = 0.8) +
    theme_classic() +
    theme(
        strip.background = element_blank(),
        panel.background = 
            element_rect(fill = "grey95"),
        axis.line = 
            element_line(colour = "grey80")
    )
```
]

.pull-right[
```{r facets, echo=FALSE}
nhanes_plot <- nhanes_plot +
    facet_grid(cols = vars(SurveyYr), rows = vars(Gender))
nhanes_plot
```
]

---

## Final exercise: Add colour scheme and set theme

.pull-left[
```{r final-exercise-plot1-4, eval=FALSE}
NHANES %>% 
    filter(!is.na(Diabetes)) %>%
    ggplot(aes(y = BMI, x = Poverty, 
               colour = Diabetes)) + 
    geom_point(alpha = 0.4) + 
    geom_smooth(size = 2, method = "gam") +
    facet_grid(cols = vars(SurveyYr), 
               rows = vars(Gender)) +
    scale_color_viridis_d(end = 0.8) + #<<
    theme_classic() + #<<
    theme( #<<
        strip.background = element_blank(), #<<
        panel.background = #<<
            element_rect(fill = "grey95"), #<<
        axis.line =  #<<
            element_line(colour = "grey80") #<<
    ) #<<
```
]

.pull-right[
```{r colour-and-theme, echo=FALSE}
nhanes_plot <- nhanes_plot +
    scale_color_viridis_d(end = 0.8) +
    theme_classic() +
    theme(strip.background = element_blank(),
          panel.background = element_rect(fill = "grey95"),
          axis.line = element_line(colour = "grey80"))
nhanes_plot
```
]

---

## Final exercise: Code to create second plot

.pull-left[
```{r final-exercise-plot2, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, 
               colour = Diabetes, 
               y = TotChol)) +
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, 
                 size = 0.75) +
    facet_grid(cols = vars(Gender)) +
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol") +
    coord_flip()
```
]

.pull-right[
```{r final-exercise-plot2, echo=FALSE}
```
]

---

## Final exercise: Remove NA and set base

.pull-left[
```{r final-exercise-plot2-1, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), #<<
           !is.na(Education)) %>% #<<
    ggplot(aes(x = Education, #<<
               colour = Diabetes, #<<
               y = TotChol)) + #<<
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, 
                 size = 0.75) +
    facet_grid(cols = vars(Gender)) +
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol") +
    coord_flip()
```
]

.pull-right[
```{r base-plot2, echo=FALSE}
nhanes_plot <- NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, colour = Diabetes, 
               y = TotChol))
nhanes_plot
```
]

---

## Final exercise: Add boxplot geom

.pull-left[
```{r final-exercise-plot2-2, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, 
               colour = Diabetes, 
               y = TotChol)) +
    geom_boxplot(fill = "grey90", #<<
                 outlier.size = 0.5, #<<
                 size = 0.75) + #<<
    facet_grid(cols = vars(Gender)) +
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol") +
    coord_flip()
```
]

.pull-right[
```{r boxplot-layer, echo=FALSE}
nhanes_plot <- nhanes_plot +
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, size = 0.75)
nhanes_plot
```
]

---

## Final exercise: Add column facets

.pull-left[
```{r final-exercise-plot2-3, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, 
               colour = Diabetes, 
               y = TotChol)) +
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, 
                 size = 0.75) +
    facet_grid(cols = vars(Gender)) + #<<
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol") +
    coord_flip()
```
]

.pull-right[
```{r facets-cols, echo=FALSE}
nhanes_plot <- nhanes_plot +
    facet_grid(cols = vars(Gender))
nhanes_plot
```
]

---

## Final exercise: Add colour scheme and set theme

.pull-left[
```{r final-exercise-plot2-4, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, 
               colour = Diabetes, 
               y = TotChol)) +
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, 
                 size = 0.75) +
    facet_grid(cols = vars(Gender)) +
    scale_color_brewer(type = "qual") + #<<
    theme_minimal() + #<<
    labs(y = "Total Cholesterol") + #<<
    coord_flip()
```
]

.pull-right[
```{r color-themes-labs, echo=FALSE}
nhanes_plot <- nhanes_plot +
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol")
nhanes_plot
```
]

---

## Final exercise: Flip the x and y axes

.pull-left[
```{r final-exercise-plot2-5, eval=FALSE}
NHANES %>%
    filter(!is.na(Diabetes), 
           !is.na(Education)) %>%
    ggplot(aes(x = Education, 
               colour = Diabetes, 
               y = TotChol)) +
    geom_boxplot(fill = "grey90", 
                 outlier.size = 0.5, 
                 size = 0.75) +
    facet_grid(cols = vars(Gender)) +
    scale_color_brewer(type = "qual") +
    theme_minimal() +
    labs(y = "Total Cholesterol") +
    coord_flip() #<<
```
]

.pull-right[
```{r flip-plot, echo=FALSE}
nhanes_plot <- nhanes_plot +
    coord_flip()
nhanes_plot
```
]
